from dataclasses import dataclass


@dataclass(repr=True)
class WindowGeometry:
    width: int
    height: int

    def __str__(self) -> str:
        return f"{self.width}x{self.height}"


def window_geometry(window) -> WindowGeometry:
    return WindowGeometry(window.winfo_width(), window.winfo_height())
